import json

import os
import argparse
import sys
import numpy as np
import h5py
import keras
from sklearn.metrics import mean_squared_error 
from sklearn.metrics import roc_curve, auc
from numpy.linalg import norm


def GetParser():
    """Argparse option for GRID hyperparameter scan script."""
    parser = argparse.ArgumentParser(description=""" Options for the hyper
                                     parameter optimisation""")

    parser.add_argument('--configs', type=str)
    parser.add_argument('--trainingfile', type=str)
    parser.add_argument('--validationfile', type=str)
    parser.add_argument('--testfile', type=str)
    parser.add_argument('--validation_config', type=str)
    parser.add_argument('--variables', type=str)
    parser.add_argument('--outputfile', type=str)
    parser.add_argument('--large_file', action='store_true')
    parser.add_argument('--epochs', type=int, default=130)

    split = parser.add_mutually_exclusive_group()
    split.add_argument('--use_gpu', action='store_true', help='''Optionto use
                       GPUs.''')

    args = parser.parse_args()
    return args


os.environ['KERAS_BACKEND'] = 'tensorflow'

args = GetParser()
use_gpu = args.use_gpu
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
'''
dc = {'GPU': 1 if use_gpu else 0, 'CPU': 1}
print('dev', dc)

config = tf.ConfigProto(device_count=dc)
config.gpu_options.per_process_gpu_memory_fraction = 0.95
set_session(tf.Session(config=config))
'''
from keras.layers import BatchNormalization
from keras.layers import Dense, Activation, Input, LeakyReLU
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau, Callback


def GetPerformance(y_pred, y_true):

    norm_bkg_test=[]
    for i in range(y_true.shape[0]//2):
        norm_bkg_test += [norm(y_true[i,:]-y_pred[i,:])]
    
    norm_sig=[]
    for i in range(y_true.shape[0]//2, y_true.shape[0]):
        norm_sig += [norm(y_true[i,:]-y_pred[i,:])]
        
    npertype = y_true.shape[0]//2

    target = np.concatenate((np.zeros(npertype),np.ones(npertype)))
    scores = np.concatenate((norm_bkg_test, norm_sig))  
                   
    fp, vp, thresholds = roc_curve(target,scores)
    roc_auc = auc(fp, vp)
        
    return roc_auc,mean_squared_error(y_pred, y_true)


class MyCallback(Callback):
    def __init__(self, X_valid):
        self.result = {}
        self.dict_list = []
        self.X_valid = X_valid


    def on_epoch_end(self, epoch, logs=None):
        y_pred = self.model.predict(self.X_valid, batch_size=5000)
        auc, mse = GetPerformance(y_pred, self.X_valid)
        dict_epoch = {
            "epoch": epoch,
            "loss": float(logs['loss']),
            "val_loss": float(logs['val_loss']),
            "mse": mse,
            "AUC:": auc
        }
        self.dict_list.append(dict_epoch)

    def on_train_end(self, logs=None):
        min_val_loss = 99
        for i, elem in enumerate(self.dict_list):
            if elem['val_loss'] < min_val_loss:
                min_val_loss = elem['val_loss']
                self.result = elem
        self.result = {"best": self.result, "all": self.dict_list}


def data_loader(trainingfile, validationfile, testfile,
                variables):
    """loads the data to memory."""


    train_file = h5py.File(trainingfile,'r')
    valid_file = h5py.File(validationfile,'r')
    X_train = train_file['train'][:]
    X_valid = valid_file['valid'][:]
    train_file.close()
    valid_file.close()
    
    test_file = h5py.File(testfile,'r')
    X_test = test_file['test'][:]
    test_file.close()


    return X_train, X_valid, X_test


def NN_train(config, X_train, X_valid, X_test):
    inputs = Input(shape=(X_valid.shape[1],))
    x = inputs
    for i, unit in enumerate(config["units"]):
        x = Dense(units=unit, activation="linear",
                  kernel_initializer='glorot_uniform')(x)
        if config["activations"][i] == 'linear':
            x = LeakyReLU(0.1)(x)
        else:
            x = Activation(config["activations"][i])(x)
 
    for i, unit in reversed(list(enumerate(config["units"]))):
        if i == len(config["units"])-1:
            continue
        x = Dense(units=unit, activation="linear",
                  kernel_initializer='glorot_uniform')(x)
        if config["activations"][i] == 'linear':
            x = LeakyReLU(0.1)(x)
        else:
            x = Activation(config["activations"][i])(x)
    predictions = Dense(units=X_valid.shape[1], activation="linear",
                  kernel_initializer='glorot_uniform')(x)
    predictions = Activation(config["activations"][len(config["units"])-1])(predictions)
    model = Model(inputs=inputs, outputs=predictions)
    model.summary()

    model_optimizer = Adam(lr=config["lr"])
    model.compile( loss='mse',
        optimizer=model_optimizer)
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2,
                                  patience=5, min_lr=0.00001)
    my_callback = MyCallback(X_test)

    callbacks = [reduce_lr, my_callback]

    model.fit(X_train, X_train,
                validation_data=[X_valid, X_valid],
                batch_size=int(config["batch_size"]), epochs=args.epochs,
                callbacks=callbacks
                )

    del model
    return my_callback.result


def train_config(hash, result):

    return {'performances': result,
            'config': hash}


def train():
    print("load data")
    X_train, X_valid, X_test = data_loader(args.trainingfile,
                                                     args.validationfile,
                                                     args.testfile,
                                                     args.variables)
    print("Read in config files")
    configs = []
    for f in args.configs.split(','):
        with open('%s' % f) as config:
            configs += json.load(config)

    print("Start HP training")
    results = []
    for i, config in enumerate(configs):
        if i % 10 == 0:
            print("Training", i, "/", len(configs))
        result = NN_train(config, X_train, X_valid, X_test)
        result = train_config(config['hash'], result)
        results.append(result)
        with open(args.outputfile, 'w') as outfile:
            json.dump(results, outfile, indent=4)


if __name__ == '__main__':
    train()
