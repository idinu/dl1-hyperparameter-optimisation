#!/bin/bash
DATASET=user.${RUCIO_ACCOUNT}.LHCOlympics.HLFae.configs
DISK=UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio add-dataset ${DATASET}

UPLOADBASE=HLFae_config

for i in `seq 0 0`;
do
  CONFIG=config_new_${i}.json
  rucio upload ${CONFIG} --name ${UPLOADBASE}_${CONFIG} --rse ${DISK}
  rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${UPLOADBASE}_${CONFIG}

done
