FROM tensorflow/tensorflow:latest-gpu-py3

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip install sklearn && \
    pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install matplotlib && \
    pip install pandas && \
    pip install papermill && \
    mkdir /afs

RUN apt-get update && apt-get install -y nano python3-tk


COPY ./train /train
WORKDIR /train

##ENV PYTHONPATH "${PYTONPATH}:./"

## run jupyter notebook by default unless a command is specified
CMD ["jupyter", "notebook", "--ip", "0.0.0.0", "--no-browser", "--allow-root"]
